from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Usuario


class CustomUserAdmin(UserAdmin):
    fieldsets = ()
    add_fieldsets = (
        (None, {
            'fields': ('empresa', 'nombres', 'apellidos', 'email',
                       'telefono', 'usuario', 'password1', 'password2'),
        }),
    )
    list_display = ('id', 'empresa', 'usuario', 'nombres', 'apellidos',
                    'email', 'telefono', 'is_staff', 'is_superuser',
                    'is_active',)
    list_filter = ('is_active', 'is_staff', 'is_superuser')
    search_fields = ('id', 'usuario', 'email', 'telefono')
    ordering = ('id',)


admin.site.register(Usuario, CustomUserAdmin)
