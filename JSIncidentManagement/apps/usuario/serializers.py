from django.contrib.auth import authenticate
from rest_framework import serializers
from .models import Usuario



class UsarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ('id', 'usuario')


class UsuarioLoginSerializer(serializers.Serializer):
    usuario = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user is not None and user.is_active:
            return user
        raise serializers.ValidationError("Usuario y/o contraseña incorrecta") 
