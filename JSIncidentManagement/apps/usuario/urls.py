from django.urls import path, include
from .api import LoginAPI

urlpatterns = [
    path('login/', LoginAPI.as_view()),
]