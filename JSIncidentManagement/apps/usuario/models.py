import django.contrib.auth.models as cbase 
from django.db import models
from utils.customfields import LowerCharField, UpperCharField


RUC_EMPRESA_PRINCIPAL = "20571486670"


class Empresa(models.Model):
    ruc = models.CharField(max_length=11, null=False, blank=False, unique=True)
    razon_social = UpperCharField(max_length=80, null=False, blank=False)
    nombre_comercial = UpperCharField(max_length=80, null=False, blank=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True, editable=False)
    fecha_modificacion = models.DateTimeField(auto_now=True, null=True, blank=True)
    is_delete = models.BooleanField(default=False)

    def __str__(self):
        return self.nombre_comercial


class CustomManagerAdminstradorDB(cbase.BaseUserManager):
    def create_user(self, empresa, usuario, password, nombres,
                    apellidos, email, telefono):
        user = self.model(empresa=empresa, usuario=usuario, nombres=nombres,
                          apellidos=apellidos, email=email, telefono=telefono)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, usuario, password):
        empresa = Empresa.objects.filter(ruc=RUC_EMPRESA_PRINCIPAL)
        if empresa.count() < 1:
            empresa = Empresa(ruc=RUC_EMPRESA_PRINCIPAL, razon_social='J&S CONSULTORES S.A.C', nombre_comercial='JS CONSULTORES')
            empresa.save()
            # raise ValueError('No existe la empresa')
        else:
            empresa = empresa.first()
        user = self.create_user(empresa=empresa, usuario=usuario,
                                password=password, nombres="Usuario",
                                apellidos="Administrador",
                                email="admin@jsconsultores.com",
                                telefono="00000000")
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Usuario(cbase.AbstractBaseUser, cbase.PermissionsMixin):
    empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
    nombres = UpperCharField(max_length=60, null=False, blank=False)
    apellidos = UpperCharField(max_length=80, null=False, blank=False)
    email = models.EmailField(max_length=70, null=False, blank=False,
                              unique=True)
    telefono = models.CharField(max_length=12, null=False, blank=False)
    usuario = LowerCharField(max_length=15, unique=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True, editable=False)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    fecha_retiro = models.DateTimeField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_delete = models.BooleanField(default=False)

    USERNAME_FIELD = 'usuario'

    objects = CustomManagerAdminstradorDB()

    def get_short_name(self):
        return self.usuario

    def get_full_name(self):
        return '{} {}'.format(self.nombres, self.apellidos)