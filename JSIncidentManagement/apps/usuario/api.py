from rest_framework import generics
from rest_framework.response import Response
from knox.models import AuthToken
from .serializers import UsuarioLoginSerializer, UsarioSerializer


# Note context=self.get_serializer_context() send context of action and is possible
# send additional data

class LoginAPI(generics.GenericAPIView):
    serializer_class = UsuarioLoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        return Response({
            "usuario": UsarioSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)
        })
