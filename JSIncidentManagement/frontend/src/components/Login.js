import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Lock from '@material-ui/icons/Lock';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './style/login';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { theme } from './theme/form';
import main_logo from './img/main_logo.png';

class Login extends Component {
    render() {
        const classes = this.props.classes;
        return (
            <div className={classes.root}>
                <main className={classes.content}>
                    <form autoComplete="off">
                        <Grid container justify="center">
                            <Grid item xs={12} sm={6} md={4}>
                                <MuiThemeProvider theme={theme}>
                                    <Paper className={classes.paper}>
                                        <div className={classes.margin}>
                                            <Grid container spacing={32} alignItems="flex-end">
                                                <Grid item xs={12}>
                                                    <img src={main_logo} className={classes.logo}/>
                                                </Grid>                                            
                                                <Grid item xs={12}>
                                                    <Grid container spacing={8} alignItems="flex-end">
                                                        <Grid item>
                                                            <AccountCircle />
                                                        </Grid>
                                                        <Grid item xs>
                                                            <TextField 
                                                                id="input-with-icon-grid" 
                                                                label="Usuario"
                                                                fullWidth
                                                            />
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Grid container spacing={8} alignItems="flex-end">
                                                        <Grid item>
                                                            <Lock />
                                                        </Grid>
                                                        <Grid item xs>
                                                            <TextField 
                                                                id="input-with-icon-grid"
                                                                label="Contraseña" 
                                                                type="password"                                                    
                                                                fullWidth
                                                            />
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Button variant="contained" size="large" color="primary" className={classes.button} type="submit">
                                                        <b>INGRESAR</b>
                                                    </Button>  
                                                </Grid>
                                            </Grid>
                                        </div>           
                                    </Paper>
                                </MuiThemeProvider>
                            </Grid>
                        </Grid>
                    </form>
                </main>
            </div>
        );
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Login);