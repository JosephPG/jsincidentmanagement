import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
    overrides: {
        MuiFormLabel:{
            root: { 
                color: '#adacad',
                "&$focused": {
                    color: "#41a4cf"
                }
            },
        },
        MuiInput: {
            underline: {
                '&:after': {
                    borderBottom: `2px solid #41a4cf`,			
                },				
                '&:focused::after': {
                    borderBottom: `2px solid #d5d7d9`,
                },		
                '&:before': {
                    borderBottom: `1px solid #d5d7d9`,			
                },
                '&:hover:not($disabled):not($focused):not($error):before': {
                    borderBottom: '2px solid #d5d7d9 !important',
                },
                '&$disabled:before': {
                    borderBottom: `1px dotted #d5d7d9`,
                },
            }
        },
        MuiButton: {
            containedPrimary: {
                'background-color': '#2c6aa7',
            }
        }
    }
});