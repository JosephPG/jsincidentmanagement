from django.db import models


class UpperCharField(models.CharField):
    """ Field for convert string to upper """

    def __init__(self, *args, **kwargs):
        super(UpperCharField, self).__init__(*args, **kwargs)

    def get_prep_value(self, value):
        value = super(UpperCharField, self).get_prep_value(value)
        return value.upper()


class LowerCharField(models.CharField):
    """ Field for convert string to upper """

    def __init__(self, *args, **kwargs):
        super(LowerCharField, self).__init__(*args, **kwargs)

    def get_prep_value(self, value):
        value = super(LowerCharField, self).get_prep_value(value)
        return value.lower()