### Django and React Integration

Dillinger uses a number of open source projects to work properly:

1. Install in django: pip install django-webpack-loader
2. Install in react: npm install webpack-bundle-tracker --save-dev
3. Create dir "templates" in the root of django project
4. Inside "templates" create index.html:
    ```html
    {% load render_bundle from webpack_loader %}
    <html>
        <head>
            <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width" />
            <title>Integration</title>
        </head>
        <body>
            <div id="root"></div>
            {% render_bundle 'main' %}
        </body>
    </html>
    ```
5. Django: in settings.py add app webpack-loader:
    
    ```py
    INSTALLED_APPS = [
        # ...Other apps...
        'webpack_loader',
    ]
    ```

6. Add template path in setting.py:

    ```py
    TEMPLATES = [
        {
            # ... other settings
            'DIRS': [os.path.join(BASE_DIR, "templates"), ],
            # ... other settings
        },
    ]
    ```

7. In settings.py add:

    ```py
    WEBPACK_LOADER = {
        'DEFAULT': {
                'BUNDLE_DIR_NAME': 'bundles/',
                'STATS_FILE': os.path.join('{}/frontend/'.format(BASE_DIR), 'webpack-stats.dev.json'),
            }
        # stats_file is where generate webpack-stats.dev.json
    }
    ```

8. In urls.py root add:

    ```py
    from django.views.generic import TemplateView
    urlpatterns = [
        # Other urls
        path('index/', TemplateView.as_view(template_name="index.html")),
    ]
    ```

9. Run, First time django throw exception: Not found webpack-stats.dev.json, because this has not been generated.
10. In .../node_modules/react-scripts/config/paths.js add:

    ```js
    module.exports = {
        // Others
        statsRoot: resolveApp('../'),
    };
    ```

11. In .../node_modules/react-scripts/config/webpack.config.dev.js add:

    ```js
    // Others const headers
    const BundleTracker = require('webpack-bundle-tracker');
    ```

12. Still in webpack.config.dev.js change publicPath and publicUrl by:
    ```js
    const publicPath = 'http://localhost:3000/';
    const publicUrl = 'http://localhost:3000/';
    ```

13. Still in webpack.config.dev.js discomment require.resolve('webpack-dev-server/client') and 
    require.resolve('webpack/hot/dev-server') and comment require.resolve('react-dev-utils/webpackHotDevClient') and 
    add plugin new BundleTracker({path: paths.statsRoot, filename: 'webpack-stats.dev.json'}):

    ```js
    module.exports = {
        entry: [
            // ... KEEP OTHER VALUES
            // this will be found near line 30 of the file
            require.resolve('webpack-dev-server/client') + '?http://localhost:3000',
            require.resolve('webpack/hot/dev-server'), 
            // require.resolve('react-dev-utils/webpackHotDevClient'), <- Update Comment this
        ],
        plugins: [
            // this will be found in the bottom of the file
            // ... other plugins
            new BundleTracker({path: paths.statsRoot, filename: 'webpack-stats.dev.json'}), 
        ],
    }
    ```

14. Restart react server and django project and test
15. If page is in white, in .../node_modules/react-scripts/config/webpack.config.dev.js in this way update:

    ```js
    module.exports = {
        //...
        optimization: {
            // ...
            // Automatically split vendor and commons
            // ...
            splitChunks: {
                chunks: 'async', // from 'all'
                name: true, // from 'false'
            },
            // ...
            runtimeChunk: false //from 'true'
        }
    }
    ```

16. Run
